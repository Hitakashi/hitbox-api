# Player Config API
***


| Endpoint | Description |
| ---- | --------------- |
| [GET /player/config/:media_type/:user_id](/media/player_config.md#get-playerconfigmedia_typeuser_id) | Returns player config for  live video |
| [GET /player/reconfig/:channe/:rec_session](/media/player_config.md#get-playerrecconfigchannelrec_session) | Returns player config for recordings. |

## `GET /player/config/:media_type/:user_id`

Returns video player information, could be useful to some people.

`user_id` can be replaced with `channel` if `media_type` is live.

| Parameter | Required? | Type | Description |
| --- | --- | --- | --- |
| authToken | No | string | User's Auth Token |

### Example URL

https://www.hitbox.tv/api/player/config/live/test-account

### Example Response 

Live:

```javascript
{
   "key":"#$54d46ea213123213979",
   "play":null,
   "clip":{
      "autoPlay":true,
      "autoBuffering":true,
      "bufferLength":"2",
      "eventCategory":"test-account/live/278723",
      "baseUrl":null,
      "url":"test-account",
      "stopLiveOnPause":true,
      "live":true,
      "smoothing":true,
      "provider":"rtmp",
      "scaling":"fit",
      "bitrates":[
         {
            "url":"test-account",
            "bitrate":3500,
            "label":"Source",
            "isDefault":true,
            "provider":"rtmpHitbox"
         }
      ],
      "controls":false,
      "type":"video",
      "adsPlayed":false
   },
   "playlist":[
      {
         "provider":"rtmp",
         "netConnectionUrl":"rtmp://fml.B6BF.edgecastcdn.net/20B6BF",
         "rtmpSubscribe":true,
         "bitrates":[
            {
               "url":"test-account",
               "bitrate":3500,
               "label":"Source",
               "isDefault":true,
               "provider":"rtmp"
            }
         ]
      }
   ],
   "plugins":{
      "rtmp":{
         "url":"flowplayer.rtmp-3.2.12.1.swf",
         "netConnectionUrl":"rtmp://edge.live.hitbox.tv/live",
         "reconnecting":true
      },
      "hls":{
         "url":"flashlsFlowPlayer.swf",
         "hls_lowbufferlength":1,
         "hls_minbufferlength":4,
         "hls_maxbufferlength":60,
         "hls_startfromlowestlevel":true,
         "hls_seekfromlowestlevel":true,
         "hls_live_flushurlcache":false,
         "hls_seekmode":"KEYFRAME",
         "hls_usehardwarevideodecoder":false
      },
      "rtmpHitbox":{
         "url":"flowplayer.rtmp-3.2.12.1.swf",
         "netConnectionUrl":"rtmp://edge.live.hitbox.tv/live",
         "reconnecting":true
      },
      "pingback":{
         "url":"flowplayer.pingback-3.2.7.swf",
         "server_url":"http://api.mawire.com/pingback",
         "video_id":"1",
         "wsUrl":"ws://54.205.241.225/viewers",
         "wsChannel":"test-account",
         "wsToken":"312312312312321321312312",
         "userName":"test-account",
         "uuid":"312312312312321321312312",
         "countryCode":"",
         "debug":true,
         "viewersPluginName":"viewers",
         "infoPluginName":"info",
         "ovaPluginName":"ova",
         "isSubscriber":false,
         "isFollower":true,
         "updateTimeout":1000,
         "embed":false
      },
      "controls":null,
      "info":{
         "display":"none",
         "url":"flowplayer.content-3.2.8.swf",
         "html":"</p>",
         "width":"50%",
         "height":30,
         "backgroundColor":"#1A1A1A",
         "backgroundGradient":"none",
         "opacity":"1",
         "borderRadius":10,
         "borderColor":"#999999",
         "border":0,
         "color":"#FFFFFF",
         "bottom":60,
         "zIndex":"10",
         "closeButton":true,
         "style":{
            "p":{
               "fontSize":16,
               "fontFamily":"verdana,arial,helvetica",
               "fontWeight":"normal"
            }
         }
      },
      "gatracker":{
         "url":"flowplayer.analytics-3.2.9.1.swf",
         "event":{
            "all":true
         },
         "debug":false,
         "accountId":"UA-42900118-2"
      },
      "ova":{
         "url":"flowplayer.liverail-3.2.7.4.swf",
         "LR_PUBLISHER_ID":208741,
         "LR_SCHEMA":"vast2-vpaid",
         "LR_ADUNIT":"in",
         "LR_VIDEO_POSITION":0,
         "LR_AUTOPLAY":1,
         "LR_CONTENT":6,
         "LR_TITLE":"test-account",
         "LR_VIDEO_ID":"18730",
         "LR_MUTED":0,
         "CACHEBUSTER":1420167259,
         "TIMESTAMP":1420167259,
         "LR_LAYOUT_SKIN_MESSAGE":"Advertisement: Stream will resume in {COUNTDOWN} seconds.",
         "LR_LIVESTREAM":1,
         "LR_LAYOUT_SKIN_ID":2,
         "LR_LAYOUT_LINEAR_PAUSEONCLICKTHRU":0,
         "LR_BITRATE":"high",
         "LR_VIDEO_URL":"http://www.hitbox.tv/test-account",
         "LR_DESCRIPTION":"test-account",
         "LR_IP":"X.X.X.X"
      }
   },
   "cdns":[
      {
         "provider":"rtmp",
         "netConnectionUrl":"rtmp://fml.B6BF.edgecastcdn.net/20B6BF",
         "rtmpSubscribe":true,
         "bitrates":[
            {
               "url":"test-account",
               "bitrate":3500,
               "label":"Source",
               "isDefault":true,
               "provider":"rtmp"
            }
         ]
      },
      {
         "provider":"rtmp",
         "netConnectionUrl":"rtmp://hitboxna06livefs.fplive.net/hitboxna06live-live",
         "rtmpSubscribe":true,
         "bitrates":[
            {
               "url":"stream_test-account",
               "bitrate":3500,
               "label":"Source",
               "isDefault":true,
               "provider":"rtmp"
            }
         ]
      },
      {
         "provider":"rtmpHitbox",
         "netConnectionUrl":"rtmp://edge.live.hitbox.tv/live",
         "rtmpSubscribe":false,
         "bitrates":[
            {
               "url":"test-account",
               "bitrate":3500,
               "label":"Source",
               "isDefault":true,
               "provider":"rtmpHitbox"
            }
         ]
      }
   ],
   "canvas":{
      "backgroundGradient":"none"
   },
   "log":{
      "level":"debug",
      "filter":"org.osmf*"
   },
   "showErrors":false,
   "settings":{
      "media_id":"-1",
      "max_buffer_count":"6",
      "buffer_length":"2",
      "max_roundtrips":"3",
      "reset_timeout":"60000",
      "play_timeout":"15000",
      "start_timeout":"10000",
      "ad_plugin":"liverail-off",
      "default_br":null,
      "enabled":"1",
      "delBitrates":[

      ]
   }
}
```

Video:

```javascript
{
   "key":"#$54d46ea442f0438979",
   "play":null,
   "clip":{
      "autoPlay":true,
      "autoBuffering":true,
      "bufferLength":"2",
      "eventCategory":"test-account/video/22222",
      "baseUrl":null,
      "url":"http://edge.hls.vods.hitbox.tv/static/videos/vods/test-account/5a1c498ed443437e74b416a97c8b80f35fdfd29-53d30943438a20/test-account/index.m3u8",
      "stopLiveOnPause":true,
      "live":false,
      "smoothing":true,
      "provider":"pseudo",
      "scaling":"fit",
      "bitrates":[
         {
            "url":"/test-account/5a1c498ed458b0b7e74b416a97c8b323235fdfd29-53d309a4d8a20/test-account/index.m3u8",
            "bitrate":0,
            "label":"HD 720p",
            "provider":"rtmpHitbox",
            "isDefault":true
         }
      ],
      "controls":false,
      "type":"video",
      "adsPlayed":false
   },
   "plugins":{
      "pseudo":{
         "url":"flashlsFlowPlayer.swf",
         "hls_debug":false,
         "hls_debug2":false,
         "hls_lowbufferlength":5,
         "hls_minbufferlength":10,
         "hls_maxbufferlength":60,
         "hls_startfromlowestlevel":false,
         "hls_seekfromlowestlevel":false,
         "hls_live_flushurlcache":false,
         "hls_seekmode":"SEGMENT"
      },
      "controls":null,
      "info":{
         "display":"none",
         "url":"flowplayer.content-3.2.8.swf",
         "html":"<p align=\"center\"><\/p>",
         "width":"50%",
         "height":30,
         "backgroundColor":"#1A1A1A",
         "backgroundGradient":"none",
         "opacity":"1",
         "borderRadius":10,
         "borderColor":"#999999",
         "border":0,
         "color":"#FFFFFF",
         "bottom":60,
         "zIndex":"10",
         "closeButton":true,
         "style":{
            "p":{
               "fontSize":16,
               "fontFamily":"verdana,arial,helvetica",
               "fontWeight":"normal"
            }
         }
      },
      "gatracker":{
         "url":"flowplayer.analytics-3.2.9.1.swf",
         "event":{
            "all":true
         },
         "debug":false,
         "accountId":"UA-42900118-2"
      },
      "ova":{
         "url":"flowplayer.liverail-3.2.7.4.swf",
         "LR_PUBLISHER_ID":20341,
         "LR_SCHEMA":"vast2-vpaid",
         "LR_ADUNIT":"in",
         "LR_VIDEO_POSITION":0,
         "LR_AUTOPLAY":1,
         "LR_CONTENT":6,
         "LR_TITLE":"5a1c498ed458b0b7e74b4312321312312fdfd29-53d309a4d8a20",
         "LR_VIDEO_ID":"180572",
         "LR_MUTED":0,
         "CACHEBUSTER":1420168000,
         "TIMESTAMP":1420168000,
         "LR_LAYOUT_SKIN_MESSAGE":"Advertisement: Stream will resume in {COUNTDOWN} seconds.",
         "LR_LIVESTREAM":1,
         "LR_LAYOUT_SKIN_ID":2,
         "LR_LAYOUT_LINEAR_PAUSEONCLICKTHRU":0,
         "LR_BITRATE":"high",
         "LR_VIDEO_URL":"http://www.hitbox.tv/video/11111",
         "LR_DESCRIPTION":"5a1c498ed4312321321312416a97c8b80f35fdfd29-53d309a4d8a20",
         "LR_IP":"X.X.X.X"
      }
   },
   "canvas":{
      "backgroundGradient":"none"
   },
   "log":{
      "level":"debug",
      "filter":"org.osmf*"
   },
   "showErrors":false,
   "settings":{
      "media_id":"-1",
      "max_buffer_count":"6",
      "buffer_length":"2",
      "max_roundtrips":"3",
      "reset_timeout":"60000",
      "play_timeout":"15000",
      "start_timeout":"10000",
      "ad_plugin":"liverail-off",
      "default_br":null,
      "enabled":"1"
   },
   "playlist":[

   ]
}
```

## `GET /player/recconfig/:channel/:rec_session`

Returns player config for recordings. `rec_session` can be taken from the [recording](/video/recordings.md) API.

### Example URL 

https://www.hitbox.tv/api/player/recconfig/test-account/312312312321312321312312312312-54b3122b336c7

### Example Response

```javascript
{
   "key":"#$54d46eaa11fsdfdsf32279",
   "play":null,
   "clip":{
      "autoPlay":true,
      "autoBuffering":true,
      "url":"http://edge.hls.vods.hitbox.tv/static/videos/vods/test-account/bb9304c4951edasdasdas02a5a937e527ac196-54b3122b336c7/test-account/index.m3u8",
      "provider":"httpstreaming",
      "scaling":"fit",
      "start":1
   },
   "plugins":{
      "controls":null,
      "httpstreaming":{
         "url":"flashlsFlowPlayer.swf",
         "hls_debug":false,
         "hls_debug2":false,
         "hls_lowbufferlength":1,
         "hls_minbufferlength":3,
         "hls_maxbufferlength":60,
         "hls_startfromlowestlevel":false,
         "hls_seekfromlowestlevel":false,
         "hls_live_flushurlcache":false,
         "hls_seekmode":"SEGMENT"
      }
   },
   "canvas":{
      "backgroundGradient":"none"
   },
   "log":{
      "level":"debug",
      "filter":"org.osmf.*, org.electroteque.m3u8.*, org.flowplayer.bitrateselect.*"
   }
}
```
